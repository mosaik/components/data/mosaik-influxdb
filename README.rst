Warning: This is outdated and only works with mosaik 2 and InfluxDB 1. If you use InfluxDB 2 and mosaik 3 or above, we recommend using the InfluxDB2 adapter you can find here: https://gitlab.com/mosaik/components/data/mosaik-influxdb2

Description
-----------
A simulator to store data from mosaik in InfluxDB 1.